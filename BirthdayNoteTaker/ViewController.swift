//
//  ViewController.swift
//  BirthdayNoteTaker
//
//  Created by Rumeysa Bulut on 10.11.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//  From the course "iOS 13 & Swift 5: Başlangıçtan İleri Seviyeye Mobil Uygulama" by Atıl Samancıoğlu

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storedName = UserDefaults.standard.object(forKey: "name") //any object, there may be no name at the first opening the app so it's optional
        let storedBirthday = UserDefaults.standard.object(forKey: "birthday")
        
        //Casting - as? vs as!
        //as? --> if you can, cast it
        //as! --> you must cast it
        
        if let newName = storedName as? String { // any object should be casted to String. as?: if it can be casted to String
            nameLabel.text = "Name: \(newName)"
        }
        if let newBirthday = storedBirthday as? String {
            birthdayLabel.text = "Birthday: \(newBirthday)"
        }
    }

    
    
    @IBAction func saveClicked(_ sender: Any) {
        UserDefaults.standard.set(nameTextField.text!, forKey: "name")
        UserDefaults.standard.set(birthdayTextField.text!, forKey: "birthday")
        
        nameLabel.text = "Name: \(nameTextField.text!)"
        birthdayLabel.text = "Birthday: \(birthdayTextField.text!)"
        
    }
    
    @IBAction func deleteClicked(_ sender: Any) {
        let storedName = UserDefaults.standard.object(forKey: "name")
        let storedBirthday  = UserDefaults.standard.object(forKey: "birthday")
        
        if (storedName as? String) != nil {
            UserDefaults.standard.removeObject(forKey: "name")
            nameLabel.text = "Name: "
        }
        if (storedBirthday as? String) != nil {
            UserDefaults.standard.removeObject(forKey: "birthday")
            nameLabel.text = "Birthday: "
        }
        
    }
    
}

